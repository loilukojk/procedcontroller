var url_api = "https://api-orimx-dev.vdc2.com.vn";
var table_proced = $('#table_proced').dataTable();
var table_answer = $('#table_answer').dataTable();
var table_file = $('#table_file').dataTable();

var global_questionId = '';
var global_contentId = '';
var global_pathDel = [];
var global_pathDoc = [];

var global_dataFile = [];
var id_counter = 0;

$(document).ready(function ($) {
	//datatable
	var url = "/api/proced/v1/list-proced";
	var data = '';
	tableProced(url, data);

	//clear input form
	clearInputForm();

	// Hiển thị danh sách file được chọn nút "Thêm mới question"
	$('#question_path_create').on('change', function () {
		//clear list of file name
		$('#list_of_file_name_question').html('');

		//get the file name
		var input = event.srcElement;
		var files = input.files;

		$.each(files, function (index, value) {
			//display list of file name			
			$('#list_of_file_name_question').append('<div style="margin: 3px">' + value.name + '</div>');
		})
	})

	// Hiển thị danh sách file được chọn nút "Thêm mới answer"
	$('#answer_path_create').on('change', function () {
		//clear list of file name
		$('#list_of_file_name_answer').html('');

		//get the file name
		var input = event.srcElement;
		var files = input.files;

		$.each(files, function (index, value) {
			//display list of file name			
			$('#list_of_file_name_answer').append('<div style="margin: 3px">' + value.name + '</div>');
		})
	})

	/* event submit form */
	//create question
	$('form#form-create-question').submit(function (e) {
		e.preventDefault();

		//upload questionPath để có dữ liệu trong global_pathDoc --> done: sẽ gọi api create-question
		uploadQuestionPath();
	});

	//edit question
	$('form#form-edit-question').submit(function (event) {
		event.preventDefault();

		//lấy chuỗi edited
		var query = $('#question_title_edit').val();

		//update question with edited query
		var data = '{"questionId": "' + global_questionId + '", "questionTitle": "' + query + '", "status": 1}';
		var url_api = '/api/proced/v1/edit-question';
		editQuestion(url_api, data);
	});

	//delete question
	$('form#form-delete-question').submit(function (e) {
		e.preventDefault();

		//delete question
		var data = '{"questionId": ["' + global_questionId + '"]}';
		var url_api = '/api/proced/v1/del-question';
		console.log('DELETE QUESTION:\ndata: ' + data + '\nurl: ' + url_api);
		deleteQuestion(url_api, data);
	})

	//create answer (add content)
	$('form#form-create-answer').submit(function (e) {
		e.preventDefault();

		//upload answerPath để có dữ liệu trong global_pathDoc --> done: sẽ gọi api add-content để thêm mới câu hỏi
		uploadAnswerPath();
	})

	//edit answer	
	$('form#form-edit-answer').submit(function (event) {
		event.preventDefault();

		//lấy chuỗi edited
		var query = $('#answer_content_edit').val();

		/* Tạo data cho edit answer*/
		//gắn pathDel vào data
		var data = '{ "anwserContent": "' + query + '", "pathDel": [';
		for (var i = global_pathDel.length; i--;) {
			data += '"' + global_pathDel[i] + '"';
			if (i > 0) { data += ','; }
		}

		//gắn pathDoc vào data
		data += '], "pathDoc": [';
		for (var i = global_pathDoc.length; i--;) {
			data += '"' + global_pathDoc[i] + '"';
			if (i > 0) { data += ','; }
		}

		//gắn id, contentId & status vào data
		data += '], "id": "' + global_questionId + '", "contentId": "' + global_contentId + '", "status" : 1}';

		/* update answer */
		var url_api = '/api/proced/v1/update-content';
		editAnswer(url_api, data);
	});

	//delete answer
	$('form#form-delete-answer').submit(function (event) {
		event.preventDefault();

		//delete answer
		var data = '{ "contentId": "' + global_contentId + '", "questionId": "' + global_questionId + '"}';
		var url_api = '/api/proced/v1/del-content';
		console.log('DELETE ANSWER: ' + data);
		deleteAnswer(url_api, data);
	})

	/* modal */
	//mở modal create new question
	$('.create-question').on('click', function (e) {
		e.preventDefault();
		$('#modalCreateQuestion').modal();
	});

	//mở modal edit question
	table_proced.on('click', 'button.edit_question', function (e) {
		e.preventDefault();
		var values = table_proced.api().row(this.closest('tr')).data();

		var questionId = values.questionId;
		var questionTitle = values.questionTitle;

		$("#question_title_edit").val(questionTitle);

		var url_answer = '/api/proced/v1/detail-proced/' + questionId;
		console.log('Edit Question --> questionId: ' + questionId + ' --> questionTitle: ' + questionTitle);
		tableAnswer(url_answer, '');

		$('#modalEditQuestion').modal();

		//ghi lại questionId
		global_questionId = questionId;

		//clear pathDel & pathDoc
		global_pathDel = [];
		global_pathDoc = [];
	});

	//mở modal delete question	
	table_proced.on('click', 'button.delete_question', function (e) {
		e.preventDefault();

		var values = table_proced.api().row(this.closest('tr')).data();
		var questionId = values.questionId;
		var questionTitle = values.questionTitle;

		$('#question_title_delete').html(questionTitle);

		$('#modalDeleteQuestion').modal();

		//ghi lại questionId
		global_questionId = questionId;
	});

	//mở modal create new answer
	$('.create-answer').on('click', function (e) {
		e.preventDefault();
		$('#modalCreateAnswer').modal();
	});

	//mở modal edit answer
	$('#table_answer').on('click', 'button.btn_edit_answer', function (e) {
		e.preventDefault();

		var values_answer = table_answer.api().row(this.closest('tr')).data();

		var questionId_answer = values_answer.questionId;
		var contentId_answer = values_answer.contentId;
		var answerContent_answer = values_answer.answerContent;

		$("#answer_content_edit").val(answerContent_answer);

		var url_answer = '/api/proced/v1/get-content/' + questionId_answer + '/' + contentId_answer;
		tableFile(url_answer, '');

		console.log('Edit Answer --> contentId_answer: ' + contentId_answer + ' --> answerContent: ' + answerContent_answer);

		$('#modalEditAnswer').modal();

		// Ghi lại contentId
		global_contentId = contentId_answer;

		// clear global data: pathDel, pathDoc & id_counter
		global_pathDel = [];
		global_pathDoc = [];
		id_counter = 0;
	});

	//mở modal delete answer
	$('#table_answer').on('click', 'button.btn_delete_answer', function (e) {
		e.preventDefault();

		var values_answer = table_answer.api().row(this.closest('tr')).data();

		//ghi lại contentId
		global_contentId = values_answer.contentId;

		//hiện modal
		var answerContent_answer = values_answer.answerContent;
		$('#answer_content_delete').html(answerContent_answer);
		$('#modalDeleteAnswer').modal();
	});

	//mở modal add new file path		
	$('.create-new-file').on('click', function (e) {
		e.preventDefault();

		//gọi 1 hành động click vào button 'add-new-file-from-hidden'
		document.getElementById('add-new-file-from-hidden').click();

		//gọi hàm vẽ lại table_file
		tableFile_redraw(global_dataFile);
	})

	//mở modal delete file path
	$('#table_file').on('click', 'button.btn_delete_file', function (e) {
		e.preventDefault();

		var values = table_file.api().row(this.closest('tr')).data();
		var pathId = values.pathId;

		//thêm pathId của filepath muốn xóa vào global_pathDel (Chỉ đưa những filepath cũ vào (có pathId dài))
		if (pathId.length > 4) {
			global_pathDel.push(pathId);
		}

		//remove data cục bộ trong mảng global_dataFile về filepath muốn xóa
		for (var i = global_dataFile.length; i--;) {
			if (global_dataFile[i].pathId === pathId) {
				global_dataFile.splice(i, 1);
				break;
			}
		}

		$('#table_file').DataTable().row($(this).parents('tr')).remove().draw();
	})

	/* Functions of dataTable */
	//table file: vẽ lại table ngay khi thêm file path mới
	function tableFile_redraw(data) {
		$('#table_file').dataTable({
			"data": data,
			"searching": true,
			"ordering": true,
			"order": [[1, "asc"]],
			"bDestroy": true,
			language: {
				search: "Tìm kiếm:",
				lengthMenu: "Hiển thị _MENU_ dòng",
				info: "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
				infoEmpty: "Không có dữ liệu",
				paginate: {
					first: "Đầu tiên",
					previous: "Trước",
					next: "Tiếp",
					last: "Cuối cùng"
				},
			},
			"columns": [
				{ "data": null },
				{
					data: "path",
					className: "center",
					render: function (data, type, row) {
						var filename = data.replace(/^.*[\\\/]/, '');
						var html = '<a style="color:#0000ff;" target="_blank" href=' + data + ' download>' + filename + '</a>';
						return html;
					}
				},
				{
					data: null,
					className: "center",
					defaultContent: '<button class="btn_delete_file btn btn-danger btn-sm" title="Xóa file"><i class="fa fa-trash-o"></i></button>'
				}
			],
			columnDefs: [{
				"targets": [0],
				"searchable": false,
				"orderable": false
			}]
		});
		$.fn.dataTable.ext.errMode = 'none';
		//style search
		$('.dataTables_filter').attr("style", "float: left; margin: -2px 0px 0px 11px;");
		// STT
		table_file.api().on('order.dt search.dt', function () {
			table_file.api().column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
	}

	//table file
	function tableFile(url, data) {
		table_file = $('#table_file').dataTable({
			"ajax": {
				url: url_api + url,
				type: "GET",
				async: true,
				crossDomain: true,
				dataType: "json",
				headers: {
					'Content-Type': 'application/json',
				},
				data: function (d) {
					return data;
				},
				dataSrc: function (json) {
					//dữ liệu trả về là null
					if (json.results == null) {
						alert('dataSrc json.results == null');
						return [];
					}

					//có dữ liệu để hiển thị
					var questionId = json.results[0].questionId;
					var pathDoc = json.results[0].questionContents[0].pathDoc;

					var arr = [];

					$.each(pathDoc, function (i, news) {
						arr.push({
							questionId: questionId,
							contentId: news.contentId,
							answerContent: news.answerContent,
							path: news.path,
							pathId: news.pathId
						});
					});

					//Lưu cục data vẽ table_file
					global_dataFile = arr;

					return arr;
				}
			},

			"searching": true,
			"ordering": true,
			"order": [[1, "asc"]],
			"bDestroy": true,
			language: {
				search: "Tìm kiếm:",
				lengthMenu: "Hiển thị _MENU_ dòng",
				info: "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
				infoEmpty: "Không có dữ liệu",
				paginate: {
					first: "Đầu tiên",
					previous: "Trước",
					next: "Tiếp",
					last: "Cuối cùng"
				},
			},
			"columns": [
				{ "data": null },
				{
					data: "path",
					className: "center",
					render: function (data, type, row) {
						var filename = data.replace(/^.*[\\\/]/, '');
						var html = '<a style="color:#0000ff;" target="_blank" href=' + data + ' download>' + filename + '</a>';
						return html;
					}
				},
				{
					data: null,
					className: "center",
					defaultContent: '<button class="btn_delete_file btn btn-danger btn-sm" title="Xóa file"><i class="fa fa-trash-o"></i></button>'
				}
			],
			columnDefs: [{
				"targets": [0],
				"searchable": false,
				"orderable": false
			}]
		});
		$.fn.dataTable.ext.errMode = 'none';
		//style search
		$('.dataTables_filter').attr("style", "float: left; margin: -2px 0px 0px 11px;");
		// STT
		table_file.api().on('order.dt search.dt', function () {
			table_file.api().column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
	}

	//table answer
	function tableAnswer(url, data) {
		table_answer = $('#table_answer').dataTable({
			"ajax": {
				url: url_api + url,
				type: "GET",
				async: true,
				crossDomain: true,
				dataType: "json",
				headers: {
					'Content-Type': 'application/json',
				},
				data: function (d) {
					return data;
				},
				dataSrc: function (json) {
					//dữ liệu trả về là null
					if (json.results == null) {
						alert('dataSrc json.results == null');
						return [];
					}

					//có dữ liệu để hiển thị
					var questionId = json.results[0].questionId;
					var questionContents = json.results[0].questionContents;

					var arr = [];

					$.each(questionContents, function (i, news) {
						if (news.status == "1") {
							arr.push({
								questionId: questionId,
								contentId: news.contentId,
								answerContent: news.answerContent,
								pathDoc: news.pathDoc
							});
						}
					});

					return arr;
				}
			},

			"searching": true,
			"ordering": true,
			"order": [[1, "asc"]],
			"bDestroy": true,
			language: {
				search: "Tìm kiếm:",
				lengthMenu: "Hiển thị _MENU_ dòng",
				info: "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
				infoEmpty: "Không có dữ liệu",
				paginate: {
					first: "Đầu tiên",
					previous: "Trước",
					next: "Tiếp",
					last: "Cuối cùng"
				},
			},
			"columns": [
				{ "data": null },
				{ "data": "answerContent" },
				{
					data: "pathDoc",
					className: "center",
					render: function (data, type, row) {
						var html = '';
						var path = '';
						$.each(data, function (iFile, vFile) {
							if (path == '') {
								path = vFile.path;
								var filename = path.replace(/^.*[\\\/]/, '');
								html += '<a style="color:#0000ff;" target="_blank" href=' + path + ' download>' + filename + '</a>';
							} else {
								path = vFile.path;
								var filename = path.replace(/^.*[\\\/]/, '');
								html += '<br><a style="color:#0000ff;" target="_blank" href=' + path + ' download>' + filename + '</a>';
							}
						});

						return html;
					}
				},
				{
					data: null,
					className: "center",
					defaultContent: '<button class="btn_edit_answer btn btn-primary btn-sm" title="Sửa câu trả lời"><i class="fa fa-pencil-square-o"></i></button>&nbsp;<button class="btn_delete_answer btn btn-danger btn-sm" title="Xóa câu trả lời"><i class="fa fa-trash-o"></i></button>'
				}
			],
			columnDefs: [{
				"targets": [0],
				"searchable": false,
				"orderable": false
			}]
		});
		$.fn.dataTable.ext.errMode = 'none';
		//style search
		$('.dataTables_filter').attr("style", "float: left; margin: -2px 0px 0px 11px;");
		// STT
		table_answer.api().on('order.dt search.dt', function () {
			table_answer.api().column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
	}

	//table question
	function tableProced(url, data) {
		table_proced = $('#table_proced').dataTable({
			"ajax": {
				url: url_api + url,
				type: "GET",
				async: true,
				crossDomain: true,
				dataType: "json",
				headers: {},
				data: function (d) {
					return data;
				},
				// dataSrc: 'results',
				dataSrc: function (json) {
					//dữ liệu trả về là null
					if (json.results == null) {
						alert('dataSrc json.results == null');
						return [];
					}

					//có dữ liệu để hiển thị
					var arr = [];

					$.each(json.results, function (i, item) {
						if (item.status == "1") {
							arr.push(item);
						}
					});

					return arr;
				}
			},
			"destroy": true,
			"searching": true,
			"ordering": true,
			"order": [[1, "asc"]],
			"bDestroy": true,
			language: {
				search: "Tìm kiếm:",
				lengthMenu: "Hiển thị _MENU_ dòng",
				info: "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
				infoEmpty: "Không có dữ liệu",
				paginate: {
					first: "Đầu tiên",
					previous: "Trước",
					next: "Tiếp",
					last: "Cuối cùng"
				},
			},
			"columns": [
				{ "data": null },
				{ "data": "questionTitle" },
				{
					data: "questionContents",
					className: "center",
					render: function (data, type, row) {
						var html = '<table id="answer_detail" style="width:100%">';
						$.each(data, function (iAnswer, vAnswer) {
							if (vAnswer.status === "1") {
								html += '<tr><td width="60%">' + vAnswer.answerContent + '</td>';
								html += '<td width="40%">';
								if (vAnswer.pathDoc.length != 0) {
									var path = '';
									$.each(vAnswer.pathDoc, function (iFile, vFile) {
										if (path == '') {
											path = vFile.path;
											var filename = path.replace(/^.*[\\\/]/, '');
											html += '<a style="color:#0000ff;" target="_blank" href=' + path + ' download>' + filename + '</a>';
										} else {
											path = vFile.path;
											var filename = path.replace(/^.*[\\\/]/, '');
											html += '<br><a style="color:#0000ff;" target="_blank" href=' + path + ' download>' + filename + '</a>';
										}
									});
								}
								html += '</td></tr>';
							}
						});
						html += '</table>';

						return html;
					}
				},
				{
					data: null,
					className: "center",
					defaultContent: '<button class="edit_question btn btn-primary btn-sm" title="Sửa câu hỏi"><i class="fa fa-pencil-square-o"></i></button>&nbsp;<button class="delete_question btn btn-danger btn-sm" title="Xóa câu hỏi"><i class="fa fa-trash-o"></i></button>'
				}
			],
			columnDefs: [{
				"targets": [0],
				"searchable": false,
				"orderable": false
			}]
		});
		$.fn.dataTable.ext.errMode = 'none';
		//style search
		$('.dataTables_filter').attr("style", "float: left; margin: -2px 0px 0px 11px;");
		// STT
		table_proced.api().on('order.dt search.dt', function () {
			table_proced.api().column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
	}

	/* Functions of create */
	//create question
	function createQuestion(url, data) {
		$.ajax({
			url: url_api + url,
			type: "POST",
			async: true,
			crossDomain: true,
			dataType: "json",
			headers: {
				"Content-Type": "application/json"
			},
			data: data,
			success: function (res) {
				alert('Success: Tạo mới câu hỏi thành công');
				location.reload();
			},
			error: function (res) {
				alert('Failed: Gặp lỗi ngoài ý muốn, tạo mới câu hỏi không thành công');
			}
		})
	}

	//create answer
	function createAnswer(url, data) {
		$.ajax({
			url: url_api + url,
			type: "POST",
			async: true,
			crossDomain: true,
			dataType: "json",
			headers: {
				"Content-Type": "application/json"
			},
			data: data,
			success: function (res) {
				alert('Success: Thêm mới câu trả lời thành công');
				location.reload();
			},
			error: function (res) {
				alert('Failed: Gặp lỗi ngoài ý muốn, thêm mới câu trả lời không thành công');
			}
		})
	}

	/* Functions of edit */
	// request for edit question
	function editQuestion(url, data) {
		$.ajax({
			url: url_api + url,
			type: "POST",
			async: true,
			crossDomain: true,
			dataType: "json",
			headers: {
				"Content-Type": "application/json"
			},
			data: data,
			success: function (res) {
				alert('Thao tác CHỈNH SỬA câu hỏi của bạn đã được thực hiện');
				location.reload();
			},
			error: function (res) {
				alert('Gặp lỗi ngoài ý muốn, tháo tác CHỈNH SỬA của bạn đã không được thực hiện');
			}
		})
	}

	function editAnswer(url, data) {
		$.ajax({
			url: url_api + url,
			type: "POST",
			async: true,
			crossDomain: true,
			dataType: "json",
			headers: {
				"Content-Type": "application/json"
			},
			data: data,
			success: function (res) {
				alert('Thao tác CHỈNH SỬA câu trả lời của bạn đã được thực hiện');
				location.reload();
			},
			error: function (res) {
				alert('Gặp lỗi ngoài ý muốn, tháo tác CHỈNH SỬA của bạn đã không được thực hiện');
			}
		})
	}

	/* functions of delete data */
	// request for delete question
	function deleteQuestion(url, data) {
		$.ajax({
			url: url_api + url,
			type: "DELETE",
			async: true,
			crossDomain: true,
			dataType: "json",
			headers: {
				"Content-Type": "application/json"
			},
			data: data,
			success: function (res) {
				alert('Success: Thao tác XÓA câu hỏi của bạn đã được thực hiện');
				location.reload();
			},
			error: function (res) {
				alert('Failed: Gặp lỗi ngoài ý muốn, tháo tác XÓA của bạn đã không được thực hiện');
			}
		})
	}

	// request for delete answer
	function deleteAnswer(url, data) {
		$.ajax({
			url: url_api + url,
			type: "DELETE",
			async: true,
			crossDomain: true,
			dataType: "json",
			headers: {
				"Content-Type": "application/json"
			},
			data: data,
			success: function (res) {
				alert('Thao tác XÓA câu trả lời của bạn đã được thực hiện');				
				location.reload();
			},
			error: function (res) {
				alert('Gặp lỗi ngoài ý muốn, thao tác XÓA của bạn đã không được thực hiện');
			}
		})
	}

	/* other functions */

	function clearInputForm() {
		$('#modalCreateArea').on('hidden.bs.modal', function () {
			$(".select_parent_C").empty();//xóa dữ liệu danh sách tìm kiếm dịch vụ cha
			$('.select_parent_C').hide();//ẩn div
			$("#form-create-area").trigger("reset");//xóa dữ liệu input form
		});
	}

	//upload file của create question
	async function uploadQuestionPath() {
		//clear data of global_pathDoc
		global_pathDoc = [];

		//upload file
		var vidFileLength = $("#question_path_create")[0].files.length;
		if (vidFileLength === 0) {//Không có file
			alert('Lưu ý: Bạn Không chọn file nào hết đó nha v:');
		} else {//có file		
			for (var i = $("#question_path_create").prop('files').length; i--;) {
				var file_contents = $("#question_path_create").prop('files')[i];
				var formdata = new FormData();
				formdata.append("data", file_contents);
				await $.ajax({
					url: url_api + "/api/upload/" + encodeURIComponent(file_contents.name),
					type: "POST",
					async: true,
					dataType: "json",
					crossDomain: true,
					processData: false,
					contentType: false,
					headers: {
					},
					mimeType: "multipart/form-data",
					data: formdata,
					success: async function (result) {
						var file = '';
						if (result.status == 1) {
							var file_path = result.file_path;

							var m = file_path.indexOf('https://') == 0 ? file_path.match(/^https:\/\/[^/]+/) : file_path.match(/^http:\/\/[^/]+/);
							var dm = m ? m[0] : "";
							file = file_path.replace(dm, '');
						}

						//gắn vào danh sách file path upload
						await global_pathDoc.push(file);
					},
					error: function (result) {
						alert('Failed: Upload file không thành công!');
					}
				});
			}

			//create-question chỉ khi đã upload hết file
			callApiCreateQuestion();
		}
	}

	//upload file của create answer (add content)
	async function uploadAnswerPath() {
		//clear data of global_pathDoc
		global_pathDoc = [];

		//upload file
		var vidFileLength = $("#answer_path_create")[0].files.length;
		if (vidFileLength === 0) {//Không có file
			alert('Lưu ý: Bạn Không chọn file nào hết đó nha v:');
		} else {//có file		
			for (var i = $("#answer_path_create").prop('files').length; i--;) {
				var file_contents = $("#answer_path_create").prop('files')[i];
				var formdata = new FormData();
				formdata.append("data", file_contents);
				await $.ajax({
					url: url_api + "/api/upload/" + encodeURIComponent(file_contents.name),
					type: "POST",
					async: true,
					dataType: "json",
					crossDomain: true,
					processData: false,
					contentType: false,
					headers: {
					},
					mimeType: "multipart/form-data",
					data: formdata,
					success: async function (result) {
						var file = '';
						if (result.status == 1) {
							var file_path = result.file_path;

							var m = file_path.indexOf('https://') == 0 ? file_path.match(/^https:\/\/[^/]+/) : file_path.match(/^http:\/\/[^/]+/);
							var dm = m ? m[0] : "";
							file = file_path.replace(dm, '');
						}

						//gắn vào danh sách file path upload
						await global_pathDoc.push(file);
					},
					error: function (result) {
						alert('Failed: Upload file không thành công!');
					}
				});
			}

			//create-answer (add content) chỉ khi đã upload hết file			
			callApiCreateAnswer();
		}
	}

	//upload file của edit answer
	$("#add-new-file-from-hidden").change(async function () {
		var vidFileLength = $("#add-new-file-from-hidden")[0].files.length;

		if (vidFileLength === 0) {//Không có file
			alert('Lưu ý: Bạn Không chọn file nào hết đó nha v:');
		} else {//có file
			for (var i = $("#add-new-file-from-hidden").prop('files').length; i--;) {
				var file_contents = $("#add-new-file-from-hidden").prop('files')[i];
				var formdata = new FormData();
				formdata.append("data", file_contents);
				await $.ajax({
					url: url_api + "/api/upload/" + encodeURIComponent(file_contents.name),
					type: "POST",
					async: true,
					dataType: "json",
					crossDomain: true,
					processData: false,
					contentType: false,
					headers: {
					},
					mimeType: "multipart/form-data",
					data: formdata,
					success: async function (result) {
						var file = '';
						if (result.status == 1) {
							var file_path = result.file_path;

							var m = file_path.indexOf('https://') == 0 ? file_path.match(/^https:\/\/[^/]+/) : file_path.match(/^http:\/\/[^/]+/);
							var dm = m ? m[0] : "";
							file = file_path.replace(dm, '');
						}

						//gắn vào danh sách file path upload
						global_pathDoc.push(file);

						//cập nhật mới dữ liệu cục bộ trước khi vẽ
						await global_dataFile.push({
							questionId: global_questionId,
							contentId: global_contentId,
							answerContent: '',
							path: file,
							pathId: (id_counter++).toString()
						});
					},
					error: function (result) {
						alert('Failed: Upload file không thành công!');
					}
				});
			}

			//gọi hàm vẽ lại table_file
			tableFile_redraw(global_dataFile);
		}
	});

	//hàm này sẽ được gọi sau khi upload file của createQuestion
	function callApiCreateQuestion() {
		//lấy data về câu hỏi thêm mới
		var questionTitle = $('#question_title_create').val();
		var questionContent = $('#question_content_create').val();

		//gắn danh sách file (questionPath) vào data nếu có dữ liệu
		var data = '{"questionTitle": "' + questionTitle + '", "questionContent": "' + questionContent + '","questionPath": [';
		for (var i = global_pathDoc.length; i--;) {
			data += '"' + global_pathDoc[i] + '"';
			if (i > 0) {
				data += ',';
			}
		}
		data += '], "status": "1"}';
		console.log(data);

		//create question
		var url_api = '/api/proced/v1/create-question';
		createQuestion(url_api, data);
	}

	//hàm này sẽ được gọi sau khi upload file của createAnser (add-content)
	function callApiCreateAnswer() {
		//lấy data về câu câu trả lời sẽ thêm mới
		var answerContent = $('#answer_content_create').val();

		//gắn danh sách file (pathDoc) vào data nếu có dữ liệu
		var data = '{"anwserContent": "' + answerContent + '", "pathDoc": [';
		for (var i = global_pathDoc.length; i--;) {
			data += '"' + global_pathDoc[i] + '"';
			if (i > 0) {
				data += ',';
			}
		}
		data += '], "id": "' + global_questionId + '"}';
		console.log(data);

		//add content
		var url_api = '/api/proced/v1/add-content';
		createAnswer(url_api, data);
	}

	function uuidv4() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}
});